#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_TECNO-LE8.mk

COMMON_LUNCH_CHOICES := \
    lineage_TECNO-LE8-user \
    lineage_TECNO-LE8-userdebug \
    lineage_TECNO-LE8-eng
